# Locais para eventos em Curitiba

## Locais públicos

### UTFPR

* Auditório = 419
* Miniauditório = 100
* Vídeo-conferência = 40

### UFPR

<https://www.ufpr.br/portalufpr/auditorios-da-ufpr>

* Reitoria
    * Teatro = 700
    * Anfi 100 = 150
* Setor de Tecnologia
    * auditório da administração = 260
    * auditório Léo Grossman = 100
    * auditório 1 Eng. Química = 116
    * auditório 2 Eng. Química = 109
* Setor de Exatas
    * auditório do DInf = 70
    * salas PA 7, 8 e 9 = 90
    * auditório química =
* Setor de Sociais Aplicadas
    * auditório Ulysses de Campos = 330
    * sala de conferências = 90
* Setor de Saúde
    * auditório Gralha Azul = 280
    * auditório Maurício Bissoli = 112
* SEPT
    * auditório grande = 260
    * auditório pequeno = 80
* Engenharia Floresal
    * auditório =
* Setor de Comunicação
    * auditório = 100
* Setor de Educação - Rebouças
    * auditório =

### IFPR - João Negrão

* Auditório =

### CEP - Colégio Estadual do Paraná

* Auditório = 800
* Salão Nobre = 150
* Sala = 113

### Prefeitura

* Vale do Pinhão
    * auditório = 60
    * 2 salas pequenas = 12
* Auditório Antônio Carlos Kraide - Portão Cultural = 184
* Cine Guarani = 165
* Cinemateca =
* Capela Santa Maria Espaço Cultural = 278
* Memorial de Curitiba = 144
* Teatro Paiol = 225
* Barigui
    * Salão de Atos = 350
    * Salão Barigui = 150
    * Salão do Lago = 150
    * Sala Jatobá = 35

### Canal da música

* Auditório =

## Locais privados

### IEP - Instituto de Engenharia do Paraná

* Auditório = 100
* Centro de convenções = 370
* Salão Nobre =
* Salas de aula = 20, 20, 45, 28

### Positivo - Praça Osório

* Auditório = 160
* Sala 207 = 80

### FESP

* Auditório = 400
* Miniauditório =

### UniCuritiba

* Auditório = 450
* Miniauditório = 180

### Opet Rebouças

* Auditório =

### FIEP - unidade Dr. Celso Charuri

* Auditório = 120

### FIEP - Torres

* Auditório =

### Sebrae

* Auditório =

### Jupter/CRIA

* Auditório = 70
* Sala aula =

### Aldeia Coworking

* Auditório = 70

### IBQP

* Auditório = 130

### PUC

* Auditório =

### Funpar

* Auditório 01 = 50
* Auditório 02 = 50
* Auditório 03 = 34
* Auditório 04 = 32
* Auditório 05 = 50
* Auditório 01 e 02 = 100

### Estação

* 4 x sala = 150

### Unicesumar Portão

### Senac André Barros

### Sesc Paço da Liberdade

### Espaço Torres


## Outros meetups

* Distrito Spark CWB
* Impact Hub
* Hotmilk
* Nex Coworking


## Empresas meetups

* Elaborata
* Ateliware
* Bcredi
* Cinq
* Conatbilizei
* Ebanx
* Juno
* MadeiraMadeira (data science)
* Olist
* Pipefy
* Rentcars